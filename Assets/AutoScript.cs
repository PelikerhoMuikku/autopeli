﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoScript : MonoBehaviour {

    
    Rigidbody Rigidbody;
    Transform Transform;
	// Use this for initialization
	void Start () {
        Rigidbody = GetComponent<Rigidbody>();
        Transform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.W))
        {
            Rigidbody.velocity = new Vector3(Rigidbody.velocity.x, Rigidbody.velocity.y, Math.Min(Rigidbody.velocity.z + 1, 50));
        }
        if (Input.GetKey(KeyCode.S))
        {
            Rigidbody.velocity = new Vector3(Rigidbody.velocity.x, Rigidbody.velocity.y, Math.Max(Rigidbody.velocity.z - 1, -50));
        }
        if (Input.GetKey(KeyCode.D))
        {
            Transform.Rotate(0, 0.1f, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            Transform.Rotate(0, -0.1f, 0);
        }
    }
}
